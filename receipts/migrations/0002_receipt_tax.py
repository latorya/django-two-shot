# Generated by Django 5.0.1 on 2024-01-31 20:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="receipt",
            name="tax",
            field=models.DecimalField(
                decimal_places=3, default=0.0, max_digits=10
            ),
            preserve_default=False,
        ),
    ]
